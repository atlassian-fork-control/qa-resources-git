package sample
{
	/*****************************************
	 * This is a comment
	 * -------------------
	 ****************************************/
	
	import sample.data.CSSContainer;
	import flash.events.Event;
	import flash.text.AntiAliasType;
	
	public class Greeting extends  otherClass
	{
		//*************************
		// Properties:
		
		public var css:CSSContainer;
		public var cssURL:String = "helloworld.css";
		
	
		//*************************
		// Constructor:
		public function Greetin()
		{
			// Load style sheet
			css = new CSSContainer();
			css.addEventListener(Event.COMPLETE, onCSSLoadComplete);	
			css.load(cssURL);
		}
		
		//*************************
		// Event Handling:
		private function onCSSLoadComplete(event:Event):void
		{
			formattedText = "<body>";
			formattedText += "<p><span class='headlineStyle'>Feliz Navidad, OU OU!</span></p>";
			formattedText += "<p>This example is aimed to test syntax HL, does it look ok?</p>";
			formattedText += "<p><a href='http://Stash.URL.xStash.URL.x.html?allClasses=1#htmlText' target='_self'>Click here for more details.</a></p>";
			formattedText += "</body>";
		}
		
		private function onCSSLoadError(event:IOErrorEvent):void
		{
			trace("ERROR: File failed to load.");
		}
	}
}
