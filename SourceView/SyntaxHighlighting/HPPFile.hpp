#ifndef HELLO_H
#define HELLO_H
 
class HelloWorld
{
private:
    String greeting;
 
public:
 
    void sayHello(String name);
 
    String GetGreeting() { return greeting; }
};
 
#endif
